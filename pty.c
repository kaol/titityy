#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <HsFFI.h>
#include <pty.h>
#include <signal.h>
#include <sys/types.h>
#include <stdio.h>

#include "pty.h"

struct termios orig_termios;

void disableRawMode() {
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
}

void enableRawMode() {
  tcgetattr(STDIN_FILENO, &orig_termios);
  atexit(disableRawMode);

  struct termios raw = orig_termios;
  raw.c_iflag &= ~(IXON | IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL );
  raw.c_oflag &= ~OPOST;
  raw.c_lflag &= ~(ECHO | ISIG | ICANON | IEXTEN | ECHONL );
  raw.c_cflag &= ~(CSIZE | PARENB);
  raw.c_cflag |= CS8;
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

int setPTYSize(int fd, const pid_t pid, short x, short y) {
  struct winsize ws = {.ws_row = y, .ws_col = x};
  int result;

  if ((result = ioctl(fd, TIOCSWINSZ, &ws)) != 0)
    return result;
  return kill(pid, SIGWINCH);
}
  
int getPTYSize(short *x, short *y) {
  int result;
  struct winsize ws;

  if ((result = ioctl(0, TIOCGWINSZ, &ws)) == 0) {
    *x = ws.ws_col;
    *y = ws.ws_row;
  } else {
    *x = *y = 0;
  }
  return result;
}

int openPTY(int *master, int *slave) {
  struct winsize ws;
  struct termios term;
  int result;

  if ((result = ioctl(STDIN_FILENO, TIOCGWINSZ, &ws)) != 0)
    return result;

  cfmakeraw(&term);

  return openpty(master, slave, NULL, &term, &ws);
}

int writeControlCharacter(const int fd, const pid_t pid, const char x) {
  int result;

  if (x == orig_termios.c_cc[VINTR]) {
    return kill(pid, SIGINT);
  } else if (x == orig_termios.c_cc[VQUIT]) {
    return kill(pid, SIGQUIT);
  } else if (x == orig_termios.c_cc[VSUSP]) {
    return kill(pid, SIGTSTP);
  } else if (x == orig_termios.c_cc[VSTOP]) {
    // TODO doesn't work?
    return tcflow(fd, TCIOFF);
  } else if (x == orig_termios.c_cc[VSTART]) {
    // TODO doesn't work?
    return tcflow(fd, TCION);
  }
  return write(fd, &x, 1);
}
