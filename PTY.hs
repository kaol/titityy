{-# LANGUAGE CApiFFI #-}
{-# LANGUAGE TupleSections #-}

module PTY where

import Control.Concurrent.MVar
import GHC.IO.Device
import GHC.IO.FD
import GHC.IO.Handle.FD
import Data.Int
import Foreign
import Foreign.C.Error
import Foreign.C.Types
import System.IO
import System.Posix.Types
import System.Process
import System.Process.Internals

foreign import capi "pty.h enableRawMode" enableRawMode :: IO ()
foreign import capi "pty.h getPTYSize" getPTYSize' :: Ptr Int16 -> Ptr Int16 -> IO CInt
foreign import capi "pty.h setPTYSize" setPTYSize' :: CInt -> CPid -> Int16 -> Int16 -> IO CInt
foreign import capi "pty.h openPTY" openPTY' :: Ptr CInt -> Ptr CInt -> IO CInt
foreign import capi "pty.h writeControlCharacter" writeControlCharacter' :: CInt -> CPid -> Word8 -> IO CInt

data PTY = PTY
  { handle :: Handle
  , processHandle :: ProcessHandle
  , fd :: CInt
  }

getPTYSize :: IO (Int, Int)
getPTYSize = alloca $ \a -> alloca $ \b -> do
  throwErrnoIfMinus1_ "getPTYSize" $ getPTYSize' a b
  (,) <$> (fromIntegral <$> peek a) <*> (fromIntegral <$> peek b)

setPTYSize :: PTY -> Int -> Int -> IO ()
setPTYSize pty width height = do
  pid <- phdlProcessHandle <$> readMVar (phandle $ processHandle pty)
  throwErrnoIfMinus1_ "setPTYSize" $
    setPTYSize' (fd pty) pid (fromIntegral width) (fromIntegral height)
  return ()

openPTY :: IO (CInt, Handle, Handle)
openPTY = alloca $ \a -> alloca $ \b -> do
  throwErrnoIfMinus1_ "openPTY" $ openPTY' a b
  masterFD <- peek a
  slaveFD <- peek b
  (masterFD,,)
    <$> fdToHandle masterFD
    <*> fdToHandle slaveFD

createPTYProcess :: CreateProcess -> IO PTY
createPTYProcess cp = do
  (masterFD, master, slave) <- openPTY
  (_, _, _, ph) <-
    createProcess $ cp { std_in = UseHandle slave
                       , std_out = UseHandle slave
                       , std_err = Inherit
                       }
  return $ PTY master ph masterFD

writeControlCharacter :: PTY -> Word8 -> IO ()
writeControlCharacter pty x = do
  pid <- phdlProcessHandle <$> readMVar (phandle $ processHandle pty)
  throwErrnoIfMinus1_ "writeControlCharacter" $ writeControlCharacter' (fd pty) pid x
