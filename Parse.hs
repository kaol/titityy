{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}

module Parse where

import Control.Applicative
import Control.Monad.Trans
import Control.Monad.Trans.State.Strict
import Data.Attoparsec.ByteString as A
import Data.ByteString hiding (map)
import Data.ByteString.Builder (Builder)
import qualified Data.ByteString.Builder as Builder
import qualified Data.ByteString.Char8 as C8
import qualified Data.ByteString.Lazy as L
import Data.Char
import Data.List (intersperse)
import Data.Maybe
import qualified Data.Word as W
import Text.Read (readMaybe)
import Debug.Trace

data ColorState = ColorState
  { fg :: !Int
  , bright :: !Bool
  }
  deriving (Show)

initialColors = ColorState 7 False

parser :: StateT ColorState Parser Builder
parser = do
  slurp <- lift $ A.takeWhile (/= 27)
  end <- isNothing <$> lift A.peekWord8
  if end then pure $ Builder.byteString slurp else do
    _ <- lift A.anyWord8
    c1 <- lift A.anyWord8
    let initial = Builder.byteString slurp <> Builder.word8 27 <> Builder.word8 c1
    case c1 of
      -- CSI
      91 -> do -- [
        params <- lift $ A.takeWhile isParameterByte
        end <- lift $ A.satisfy isFinalByte
        x <- case end of
          109 -> do -- m
            case params of
              "" -> do
                resetColor
                return mempty
              _ -> do
                mconcat . Data.List.intersperse (Builder.word8 59) . map Builder.byteString <$>
                  mapM controlColors (split 59 params)
          _ -> do
            return $ Builder.byteString params
        ((initial <> x <> Builder.word8 end) <>) <$> parser
      _ -> do
        (initial <>) <$> parser
  where
    isParameterByte x = x >= 0x30 && x <= 0x3f
    isFinalByte x = x >= 0x40 && x <= 0x7f
    resetColor = modify $ \s -> s { fg = 7, bright = False }
    useColor n = do
      modify $ \s -> s { fg = n }
      b <- gets bright
      return . C8.pack . show @Int $ n + if b then 90 else 30
    controlColors x = do
      case x of
        "0" -> do
          resetColor
          return x
        "1" -> do
          modify $ \s -> s { bright = True }
          c <- gets fg
          return $ C8.pack $ show @Int (c+90)
        "30" -> useColor 0
        "31" -> useColor 1
        "32" -> useColor 2
        "33" -> useColor 3
        "34" -> useColor 4
        "35" -> useColor 5
        "36" -> useColor 6
        "37" -> useColor 7
        "39" -> useColor 7
        _ -> return x

runColorParser :: ColorState -> ByteString -> A.Result (ByteString, ColorState)
runColorParser s = fmap (\(a, b) -> (L.toStrict $ Builder.toLazyByteString a, b)) .
                   A.parse (runStateT parser s)
