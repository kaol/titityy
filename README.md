# titityy

titityy is a wrapper to run another TTY using program, which
reinterprets any SGR 1 codes ("bold or increased intensity") to
instead use colors in the range 90-97.  A "TTY pipe" if you like.

The traditional way for interpreting SGR 1 on PC has been to use a
different, brighter foreground color, with no font changes.  This has
led to things like using black text on black background with SGR 1,
where the result is showing dark gray text.  Modern terminal emulators
which have had font support chose to use also (like xterm) or instead
(like some other projects) a bolder font.  The latter choice makes
programs relying on SGR 1 to change foreground font color to break,
showing black text on black background.

The authors of the terminal emulators implementing the latter behavior
generally regard it as bug that SGR 1 was ever used that way and may
not want to implement a compatibility mode.

License GPLv2 or (at your option) later
