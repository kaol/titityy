{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import qualified Data.Attoparsec.ByteString as A
import qualified Data.ByteString as BS
import Control.Concurrent
import Control.Exception hiding (handle)
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.State
import Data.Maybe
import System.Environment (getArgs)
import System.Posix.Signals
import System.Posix.Signals.Exts
import System.Posix.Terminal
import System.IO
import System.Process
import Debug.Trace

import Parse
import PTY

main :: IO ()
main = do
  enableRawMode
  hSetBuffering stdin NoBuffering
  inputterDone <- newEmptyMVar
  slaverDone <- newEmptyMVar

  args <- getArgs
  slave <- createPTYProcess $ proc (head args) args
  
  installHandler sigWINCH (Catch $ uncurry (setPTYSize slave) =<< getPTYSize) Nothing

  let inputter = do
        x <- BS.hGetSome stdin 4096
        let output b = do
              let a@(ini,(ctrl, rest)) = BS.span (<= 0x1f) <$> BS.span (> 0x1f) b
              when (not $ BS.null ini) $
                BS.hPutStr (handle slave) ini
              mapM_ (writeControlCharacter slave) $ BS.unpack ctrl
              when (not $ BS.null rest) $ output rest
        output x
          
      slaver = do
        liftIO $ hWaitForInput (handle slave) (-1)
        input <- liftIO $ BS.hGetNonBlocking (handle slave) 4096
        let useResult (A.Done i (r,s)) = do
              liftIO $ do
                BS.hPut stdout $ r <> i
              return $ Just s
            useResult (A.Partial f) = do
              input <- liftIO $ BS.hGetNonBlocking (handle slave) 4096
              useResult $ f input
            useResult (A.Fail i ctx err) = do
              liftIO $ do
                BS.hPut stdout i
              return Nothing
        s <- get
        s' <- useResult $ runColorParser s input
        maybe (return ()) put s'

  inputterThread <- forkIO $ do
    (forever inputter) `catch` \(e :: SomeException) -> return ()
    putMVar inputterDone ()
  forkIO $ do
    (forever $ runStateT slaver initialColors) `catch` \(e :: SomeException) -> return ()
    killThread inputterThread
    putMVar slaverDone ()

  takeMVar slaverDone
  return ()
