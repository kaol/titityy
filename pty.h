#pragma once

#include <sys/types.h>

extern void enableRawMode();
extern int getPTYSize(short *, short *);
extern int setPTYSize(int, const pid_t, short, short);
extern int openPTY(int *, int *);
extern int writeControlCharacter(const int, const pid_t, const char);
